package com.example.matheuskittler.dd_java.view.main;

/**
 * @author Matheus Kittler
 * @author matheus.santos@operacao.rcadigital.com.br
 * @since 18/03/2019$
 */
public class MainActivityViewModel {

	private MainActivityInteraction interaction;

	public MainActivityViewModel(MainActivityInteraction interaction){
		this.interaction = interaction;
	}
}
