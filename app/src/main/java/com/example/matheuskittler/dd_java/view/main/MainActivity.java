package com.example.matheuskittler.dd_java.view.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matheuskittler.dd_java.R;
import com.example.matheuskittler.dd_java.model.Character;
import com.example.matheuskittler.dd_java.view.CharacterActivity;
import com.example.matheuskittler.dd_java.view.account.AccountActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher, AdapterView.OnItemSelectedListener {


	private MainActivityViewModel viewModel;
	private EditText etName;
	private Spinner spRaces;
	private Spinner spClass;
	private Spinner spAntecedent;
	private Button btnCreate;
	private Button btnAccount;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle("Dungeons and dragons");



		etName = findViewById(R.id.etName);
		spRaces = findViewById(R.id.spRaces);
		spClass = findViewById(R.id.spClass);
		spAntecedent = findViewById(R.id.spAntecedents);
		btnCreate = findViewById(R.id.btnCreate);
		btnAccount = findViewById(R.id.btnAccount);
		btnCreate.setOnClickListener(this);
		btnAccount.setOnClickListener(this);
		etName.addTextChangedListener(this);
		Spinner spRace = findViewById(R.id.spRaces);
		Spinner spClass = findViewById(R.id.spClass);
		Spinner spAntecedent = findViewById(R.id.spAntecedents);

//		ArrayAdapter<CharSequence> adapter1 = (ArrayAdapter<CharSequence>) ArrayAdapter.createFromResource(this, R.array.Races, android.R.layout.simple_spinner_item);
//		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		spRace.setAdapter(adapter1);
//		spRace.setOnItemSelectedListener(this);
//
//		ArrayAdapter<CharSequence> adapter2 = (ArrayAdapter<CharSequence>) ArrayAdapter.createFromResource(this, R.array.Classes, android.R.layout.simple_spinner_item);
//		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		spClass.setAdapter(adapter2);
//		spClass.setOnItemSelectedListener(this);
//
//
//		ArrayAdapter<CharSequence> adapter3 = (ArrayAdapter<CharSequence>) ArrayAdapter.createFromResource(this, R.array.Antecedent, android.R.layout.simple_spinner_item);
//		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		spAntecedent.setAdapter(adapter3);
//		spAntecedent.setOnItemSelectedListener(this);


	}


	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnCreate:
				Intent intent = new Intent(MainActivity.this, CharacterActivity.class);
				startActivity(intent);
				break;
			case R.id.btnAccount:
				Character c = new Character(etName.getText().toString(),spRaces.getSelectedItem().toString(),spClass.getSelectedItem().toString(), spAntecedent.getSelectedItem().toString());
				Intent i = new Intent(MainActivity.this, AccountActivity.class);
				i.putExtra("character", c);
				startActivity(i);
				break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {


	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable s) {
//		btnCreate.setEnabled(verifyFields(etName)
//				&& (etName.getText().toString() == null)
//				&& (spRaces.isSelected())
//				&& (spClass.isSelected())
//				&& (spAntecedent.isSelected()));
	}


//	private boolean verifyFields(EditText ... ets){
//		for (EditText et : ets){
//			if (et.getText().toString().isEmpty()){
//				return false;
//			}
//		}
//		return true;
//	}



}// Activity


