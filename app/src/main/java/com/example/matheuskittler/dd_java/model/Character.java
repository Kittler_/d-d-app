package com.example.matheuskittler.dd_java.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;

import java.io.Serializable;

/**
 * @author Matheus Kittler
 * @author matheus.santos@operacao.rcadigital.com.br
 * @since 18/03/2019$
 */
public class Character implements Parcelable {

	private String name;
	private String race;
	private String classe;
	private String antecedent;

	public Character(String name, String race, String classe, String antecedent) {
		this.name = name;
		this.race = race;
		this.classe = classe;
		this.antecedent = antecedent;
	}


	protected Character(Parcel in) {
		name = in.readString();
		race = in.readString();
		classe = in.readString();
		antecedent = in.readString();
	}

	public static final Creator<Character> CREATOR = new Creator<Character>() {
		@Override
		public Character createFromParcel(Parcel in) {
			return new Character(in);
		}

		@Override
		public Character[] newArray(int size) {
			return new Character[size];
		}
	};

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public String getAntecedent() {
		return antecedent;
	}

	public void setAntecedent(String antecedent) {
		this.antecedent = antecedent;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeString(race);
		dest.writeString(classe);
		dest.writeString(antecedent);
	}
}
