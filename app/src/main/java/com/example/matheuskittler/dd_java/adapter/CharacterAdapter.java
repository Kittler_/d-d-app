package com.example.matheuskittler.dd_java.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.matheuskittler.dd_java.R;
import com.example.matheuskittler.dd_java.model.Character;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Matheus Kittler
 * @author matheus.santos@operacao.rcadigital.com.br
 * @since 18/03/2019$
 */
public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder> {

	private LayoutInflater inflater;
	private List<Character> characters;

	private int clickCharacter;
	private OnCharacterClickListener onCharacterClickListener;

	public CharacterAdapter(Context context) {
		this.inflater = LayoutInflater.from(context);
		this.characters = new ArrayList<>();
	}

	public void addAccount(Character character){
		this.characters.add(character);
		notifyDataSetChanged();
	}

	@NonNull
	@Override
	public CharacterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		return new CharacterViewHolder(inflater.inflate(R.layout.row_character, viewGroup, false));
	}

	@Override
	public void onBindViewHolder(@NonNull CharacterViewHolder ViewHolder, int i) {
		Character character = characters.get(i);
		CharacterViewHolder characterViewHolder = ViewHolder;
		characterViewHolder.tvName.setText((CharSequence) character.getName());
		characterViewHolder.tvRace.setText((CharSequence) character.getRace());
		characterViewHolder.tvClass.setText((CharSequence) character.getClasse());
		characterViewHolder.tvAntecedent.setText((CharSequence) character.getAntecedent());

		characterViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

			}
		});
	}


	@Override
	public int getItemCount() {
		return characters.size();
	}

	public void setCharacters(List<Character> characters){
		this.characters = characters;
		notifyDataSetChanged();
	}



	class CharacterViewHolder extends RecyclerView.ViewHolder{

		private TextView tvName;
		private TextView tvRace;
		private TextView tvClass;
		private TextView tvAntecedent;

		public CharacterViewHolder(@NonNull View itemView){
			super(itemView);
			tvName = itemView.findViewById(R.id.tvName);
			tvRace = itemView.findViewById(R.id.tvRace);
			tvClass = itemView.findViewById(R.id.tvClass);
			tvAntecedent = itemView.findViewById(R.id.tvAntecedent);
		}
	}

	public interface OnCharacterClickListener{
		void OnCharacterClick (Character character);
	}

}

