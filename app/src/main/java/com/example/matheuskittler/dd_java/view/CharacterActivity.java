package com.example.matheuskittler.dd_java.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.example.matheuskittler.dd_java.R;


public class CharacterActivity extends AppCompatActivity {

	private WebView wvSite;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_character);
		setTitle("Infos");

		setContentView(R.layout.activity_character);

		wvSite = findViewById(R.id.wvSite);
		wvSite.loadUrl("https://dnd5e.fandom.com/wiki/D%26D_5th_Edition_Wikia");
	}
}
