package com.example.matheuskittler.dd_java.view.account;

import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.matheuskittler.dd_java.R;
import com.example.matheuskittler.dd_java.adapter.CharacterAdapter;
import com.example.matheuskittler.dd_java.model.Character;

public class AccountActivity extends AppCompatActivity implements AccountActivityInteraction {

	private AccountViewModel viewModel;
	private CharacterAdapter adapter;
	private RecyclerView rvCharacter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account);

		viewModel = new AccountViewModel((AccountActivityInteraction) this);

		getSupportActionBar().setTitle("Fichas");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		rvCharacter = findViewById(R.id.rvCharacter);
		adapter = new CharacterAdapter(this);
		rvCharacter.setAdapter(adapter);
		rvCharacter.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));

		Character character = getIntent().getParcelableExtra("character");
		adapter.addAccount(character);
	}
}
